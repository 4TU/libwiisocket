## libwiisocket
This library provides standard bsd socket headers and functions for the Ninetndo Wii  

### Status
The library successfully initializes and some common functions work correctly,  
however no in depth testing has been performed, and some functions might behave  
incorrectly or crash.  

### Building
DevkitPPC and libogc must be installed.  
To build the library, run `make`  

### Installing
To install the library, run `make install`  

### Usage
To use this library in your project, you must link
the `libwiisocket` library (`-lwiisocket`).  
You also shouldn't include the `<network.h>` header provided by
libogc, as it'll conflict with the standard headers included in this
library.  
The library must be linked *before* libogc, as some of the standard
socket functions provided by this library are also dedefined in libogc
with not standard types.  
  
The `<wiisocket.h>` header contains library initialization/management prototypes.  
Usage of those functions is documented inside of the [header](include/wiisocket.h).  

### License
GNU GPL v2.0+  
  
### This library is based on
* libogc socket implementation  
* Dolphin Emulator sockets code  
* OpenBSD socket functions and headers  
* bionic libc  
  
### Credits
* rw-r-r-0644  
* CompuCat for reminding rw about this project ;D  
  
