#include "wiisocket_internal.h"

struct SO_REQUEST SendToRequest
{
    int sockfd;
    int flags;
    int hasAddr;
    struct sockaddr_storage addr;
};

ssize_t
sendto(int sockfd,
       const void *buf,
       size_t len,
       int flags,
       const struct sockaddr *dest_addr,
       socklen_t addrlen)
{
    struct SendToRequest request;
    void *__buf;
    int rc;

    if (__wiisocket_ip_top_fd < 0) {
        errno = ENXIO;
        return -1;
    }

    memset(&request, 0, sizeof(request));
    request.sockfd = __wiisocket_get_native_fd(sockfd);
    if (request.sockfd == -1) {
        errno = ENOTSOCK;
        return -1;
    }
    request.flags = flags;

    __buf = __wiisocket_malloc(len);
    if (__buf == NULL) {
        errno = ENOMEM;
        return -1;
    }
    memcpy(__buf, buf, len);

    if (dest_addr) {
        if (addrlen < sizeof(struct sockaddr_in)) {
            errno = EINVAL;
            return -1;
        }
        memcpy(&request.addr, dest_addr, sizeof(struct sockaddr_in));
        request.addr.ss_len = sizeof(struct sockaddr_in);
        request.hasAddr = 1;
    }

    rc = IOS_IoctlvFormat(__wiisocket_ios_heapid,
                          __wiisocket_ip_top_fd,
                          IOCTLV_SO_SENDTO,
                          "dd:",
                          __buf,
                          len,
                          &request,
                          sizeof(request));

    __wiisocket_free(__buf);

    return (ssize_t)__wiisocket_get_result(NULL, rc);
}
