#include "wiisocket_internal.h"

struct SO_REQUEST CloseRequest
{
    int sockfd;
};

int
__wiisocket_close(struct _reent *r,
                  void *fd)
{
    struct CloseRequest request;
    int rc;

    if (__wiisocket_ip_top_fd < 0) {
        r->_errno = ENXIO;
        return -1;
    }

    request.sockfd = *(int *)fd;

    rc = IOS_Ioctl(__wiisocket_ip_top_fd,
                   IOCTL_SO_CLOSE,
                   &request,
                   sizeof(request),
                   NULL,
                   0);

    return __wiisocket_get_result(r, rc);
}

