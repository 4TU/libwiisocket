#include "wiisocket_internal.h"

struct SO_REQUEST FcntlRequest
{
    int fd;
    int cmd;
    int flags;
};

int
fcntl(int fd,
      int cmd, ...)
{
    struct FcntlRequest request;
    va_list args;
    int rc;

    if (__wiisocket_ip_top_fd < 0) {
        errno = ENXIO;
        return -1;
    }

    memset(&request, 0, sizeof(request));
    request.fd = __wiisocket_get_native_fd(fd);
    if (request.fd == -1) {
        errno = ENOTTY;
        return -1;
    }

    if (cmd == F_GETFL) {
        request.cmd = 3;
    } else if (cmd == F_SETFL) {
        request.cmd = 4;
    } else {
        errno = EOPNOTSUPP;
        return -1;
    }

    va_start(args, cmd);
    if (va_arg(args, int) & O_NONBLOCK) {
        request.flags |= 0x04;
    }
    va_end(args);

    rc = IOS_Ioctl(__wiisocket_ip_top_fd,
                   IOCTL_SO_FCNTL,
                   &request,
                   sizeof(request),
                   NULL,
                   0);

    if ((cmd == F_SETFL) || (rc <= 0)) {
        return __wiisocket_get_result(NULL, rc);
    }

    /* F_GETFL */
    return (rc & 0x04) ? O_NONBLOCK : 0;
}
