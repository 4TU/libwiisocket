#include "wiisocket_internal.h"

struct SO_REQUEST GetHostByAddrRequestOut
{
    struct NET_HOST_ENT n_ent;
    unsigned char buf[0x450];
};

struct SO_REQUEST GetHostByAddrRequest
{
    int len;
    int type;
    unsigned char pad[4];
    struct sockaddr_storage addr;
};

struct hostent *
gethostbyaddr(const void *addr,
              socklen_t len,
              int type)
{
    static struct GetHostByAddrRequestOut output;
    struct GetHostByAddrRequest request;
    int rc;

    if (__wiisocket_ip_top_fd < 0) {
        errno = ENXIO;
        return NULL;
    }

    if (!addr) {
        errno = EFAULT;
        return NULL;
    }

    request.len = len;
    request.type = type;
    memcpy(&request.addr, addr, len);

    rc = IOS_Ioctl(__wiisocket_ip_top_fd,
                   IOCTL_SO_GETHOSTBYADDR,
                   &request,
                   sizeof(request),
                   &output,
                   sizeof(output));

    if (rc < 0) {
        __wiisocket_get_result(NULL, rc);
        return NULL;
    }

    return __wiisocket_translate_hostent(&output.n_ent);
}
