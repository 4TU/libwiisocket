#include "wiisocket_internal.h"

static devoptab_t
__wiisocket_devoptab =
{
   .name = "soc",
   .structSize   = sizeof(int),
   .open_r       = __wiisocket_open,
   .close_r      = __wiisocket_close,
   .write_r      = __wiisocket_write,
   .read_r       = __wiisocket_read,
   .seek_r       = NULL,
   .fstat_r      = NULL,
   .stat_r       = NULL,
   .link_r       = NULL,
   .unlink_r     = NULL,
   .chdir_r      = NULL,
   .rename_r     = NULL,
   .mkdir_r      = NULL,
   .dirStateSize = 0,
   .diropen_r    = NULL,
   .dirreset_r   = NULL,
   .dirnext_r    = NULL,
   .dirclose_r   = NULL,
   .statvfs_r    = NULL,
   .ftruncate_r  = NULL,
   .fsync_r      = NULL,
   .deviceData   = 0,
   .chmod_r      = NULL,
   .fchmod_r     = NULL,
   .rmdir_r      = NULL,
};


struct wiisocket_init_data
{
    lwpq_t queue;
    int res;
    
    void (*usrcb)(int res, void *usrdata);
    void *usrdata;
};


int
__wiisocket_dev = -1;

static volatile int
__wiisocket_initialised = 0;


static void
__wiisocket_init_cb(int initres,
                    void *ptr)
{
    struct wiisocket_init_data *data = (struct wiisocket_init_data *)ptr;

    /* add wiisocket devoptab */
    if (initres >= 0) {
        data->res = 0;

        __wiisocket_dev = AddDevice(&__wiisocket_devoptab);
        if (__wiisocket_dev < 0) {
            DEBUG("devoptab AddDevice failed: %d\n", __wiisocket_dev);
            __wiisocket_network_deinit();
            data->res = -4;
        }
    } else {
        DEBUG("network initialisation failed: %d\n", initres);
        data->res = -3;
    }

    /* update initialisation status */
    if (data->res >= 0) {
        __wiisocket_initialised = 1;
    } else {
        __wiisocket_initialised = 0;
    }

    /* call cb or resume thread */
    if (data->usrcb) {
        data->usrcb(data->res, data->usrdata);
        return;
    } else {
        LWP_ThreadBroadcast(data->queue);
    }
}

int
wiisocket_async_init(void (*usrcb)(int res, void *usrdata),
                     void *usrdata)
{
    static struct wiisocket_init_data data;
    int res;

    if (__wiisocket_initialised) {
        return __wiisocket_initialised;
    } else {
        __wiisocket_initialised = -1;
    }

    LWP_InitQueue(&data.queue);

    if (usrcb) {
        data.usrcb = usrcb;
        data.usrdata = usrdata;
    }

    res = __wiisocket_network_init_async(__wiisocket_init_cb,
                                         &data);
    if (res < 0) {
        DEBUG("__wiisocket_network_init_async: %d\n", res);
        res = -2;
    }
    if (!res && !usrcb) {
        DEBUG("waiting for network...\n");
        LWP_ThreadSleep(data.queue);
        res = data.res;
    }

    LWP_CloseQueue(data.queue);

    if (res < 0) {
        __wiisocket_initialised = 0;
        return res;
    }

    return 0;
}

int
wiisocket_init(void)
{
    return wiisocket_async_init(NULL, NULL);
}

void
wiisocket_deinit(void)
{
    if (!__wiisocket_initialised) {
        return;
    }

    RemoveDevice(__wiisocket_devoptab.name);
    __wiisocket_network_deinit();

    __wiisocket_initialised = 0;
}

int
wiisocket_get_status(void)
{
    return __wiisocket_initialised;
}
