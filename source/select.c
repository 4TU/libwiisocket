#include "wiisocket_internal.h"

int
select(int nfds,
       fd_set *readfds,
       fd_set *writefds,
       fd_set *exceptfds,
       struct timeval *timeout)
{
    struct pollfd *fds;
    int rc, i, found;

    fds = (struct pollfd*)__wiisocket_malloc(nfds * sizeof(struct pollfd));
    if (!fds) {
        errno = ENOMEM;
        return -1;
    }

    for (i = 0; i < nfds; i++) {
        fds[i].fd = -1;
        fds[i].events = 0;
        fds[i].revents = 0;

        if (readfds && FD_ISSET(i, readfds)) {
            fds[i].events |= POLLIN;
            fds[i].fd = i;
        }
        if (writefds && FD_ISSET(i, writefds)) {
            fds[i].events |= POLLOUT;
            fds[i].fd = i;
        }
        if (exceptfds && FD_ISSET(i, exceptfds)) {
            fds[i].fd = i;
        }
    }

    if (timeout) {
        rc = poll(fds, (nfds_t)nfds, timeout->tv_sec * 1000 + timeout->tv_usec / 1000);
    } else {
        rc = poll(fds, (nfds_t)nfds, -1);
    }

    if (rc < 0) {
        __wiisocket_free(fds);
        return rc;
    }

    rc = 0;
    for(i = 0; i < nfds; i++) {
        found = 0;

        if (fds[i].fd < 0) {
            continue;
        }
    
        if (readfds && FD_ISSET(i, readfds)) {
            if(fds[i].revents & (POLLIN | POLLHUP)) {
                found = 1;
            } else {
                FD_CLR(i, readfds);
            }
        }

        if (writefds && FD_ISSET(i, writefds)) {
            if(fds[i].revents & (POLLOUT | POLLHUP)) {
                found = 1;
            } else {
                FD_CLR(i, writefds);
            }
        }

        if (exceptfds && FD_ISSET(i, exceptfds)) {
            if(fds[i].revents & POLLERR) {
                found = 1;
            } else {
                FD_CLR(i, exceptfds);
            }
        }

        if (found) {
            rc++;
        }
    }

    __wiisocket_free(fds);
    return rc;
}
