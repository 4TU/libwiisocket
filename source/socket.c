#include "wiisocket_internal.h"

struct SO_REQUEST SocketRequest
{
    int domain;
    int type;
    int protocol;
};

int
socket(int domain,
       int type,
       int protocol)
{
    struct SocketRequest request;
    int rc, fd, rcvbuf = 32768;

    if (__wiisocket_ip_top_fd < 0) {
        errno = ENXIO;
        return -1;
    }

    if (protocol == IPPROTO_TCP) {
        if (type != SOCK_STREAM) {
            errno = EPROTOTYPE;
            return -1;
        }
        protocol = IPPROTO_IP;
    } else if (protocol == IPPROTO_UDP) {
        if (type != SOCK_DGRAM) {
            errno = EPROTOTYPE;
            return -1;
        }
        protocol = IPPROTO_IP;
    } else if (protocol != IPPROTO_IP) {
        errno = EPROTONOSUPPORT;
        return -1;
    }

    fd = __alloc_handle(__wiisocket_dev);
    if (fd == -1) {
        errno = EMFILE;
        return -1;
    }

    request.domain = domain;
    request.type = type;
    request.protocol = protocol;

    rc = IOS_Ioctl(__wiisocket_ip_top_fd,
                   IOCTL_SO_SOCKET,
                   &request,
                   sizeof(request),
                   NULL,
                   0);
    if (rc < 0) {
        __release_handle(fd);
        return __wiisocket_get_result(NULL, rc);
    }

    *(int *)__get_handle(fd)->fileStruct = rc;

    setsockopt(fd,
               SOL_SOCKET,
               SO_RCVBUF,
               (char *)&rcvbuf,
               sizeof(rcvbuf));

    return fd;
}
