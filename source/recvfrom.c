#include "wiisocket_internal.h"

struct SO_REQUEST RecvFromRequest
{
    int sockfd;
    int flags;
};

ssize_t
recvfrom(int sockfd,
         void *buf,
         size_t len,
         int flags,
         struct sockaddr *src_addr,
         socklen_t *addrlen)
{
    struct RecvFromRequest request;
    void *__buf;
    int rc;
    
    if (__wiisocket_ip_top_fd < 0) {
        errno = ENXIO;
        return -1;
    }

    if (addrlen && src_addr->sa_len != *addrlen) {
        src_addr->sa_len = *addrlen;
    }

    request.sockfd = __wiisocket_get_native_fd(sockfd);
    if (request.sockfd == -1) {
        errno = ENOTSOCK;
        return -1;
    }
    request.flags = flags;

    __buf = __wiisocket_malloc(len);
    if (__buf == NULL) {
        errno = ENOMEM;
        return -1;
    }
    memset(__buf, 0, len);

    rc = IOS_IoctlvFormat(__wiisocket_ios_heapid,
                          __wiisocket_ip_top_fd,
                          IOCTLV_SO_RECVFROM,
                          "d:dd",
                          &request,
                          sizeof(request),
                          __buf,
                          len,
                          src_addr,
                          addrlen ? *addrlen : 0);

    memcpy(buf, __buf, len);
    __wiisocket_free(__buf);

    if (addrlen && src_addr) {
        *addrlen = src_addr->sa_len;
    }

    return (ssize_t)__wiisocket_get_result(NULL, rc);
}
