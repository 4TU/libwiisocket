#include "wiisocket_internal.h"

struct SO_REQUEST AcceptRequest
{
    int sockfd;
};

int
accept(int sockfd,
       struct sockaddr *addr,
       socklen_t *addrlen)
{
    struct AcceptRequest request;
    int rc, fd;

    if (__wiisocket_ip_top_fd < 0) {
        errno = ENXIO;
        return -1;
    }

    request.sockfd = __wiisocket_get_native_fd(sockfd);
    if (request.sockfd == -1) {
        errno = ENOTSOCK;
        return -1;
    }

    if (addr) {
        if (!addrlen || (*addrlen < sizeof(struct sockaddr_in))) {
            errno = EINVAL;
            return -1;
        }
        addr->sa_len = sizeof(struct sockaddr_in);
        addr->sa_family = AF_INET;
    }

    fd = __alloc_handle(__wiisocket_dev);
    if (fd == -1) {
        errno = EMFILE;
        return -1;
    }

    rc = IOS_Ioctl(__wiisocket_ip_top_fd, 
                   IOCTL_SO_ACCEPT,
                   &request,
                   sizeof(request),
                   addr,
                   addr ? addr->sa_len : 0);

    if (rc < 0) {
        __release_handle(fd);
        return __wiisocket_get_result(NULL, rc);
    }

    if (addrlen) {
        *addrlen = addr->sa_len;
    }

    *(int *)__get_handle(fd)->fileStruct = rc;
    return fd;
}
