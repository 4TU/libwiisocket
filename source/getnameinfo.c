#include "wiisocket_internal.h"

struct SO_REQUEST GetNameInfoRequest
{
    int flags;
    int hasAddr;
    struct sockaddr_storage addr;
};

int
getnameinfo(const struct sockaddr *addr,
            socklen_t addrlen,
            char *host,
            socklen_t hostlen,
            char *serv,
            socklen_t servlen,
            int flags)
{
    int rc;
    struct GetNameInfoRequest request;
    char *__host = NULL, *__serv = NULL;

    if (__wiisocket_ip_top_fd < 0) {
        errno = ENXIO;
        return EAI_SYSTEM;
    }

    if (host) {
        __host = __wiisocket_malloc(hostlen);
        if (!__host) {
            rc = EAI_MEMORY;
            goto end;
        }
    }
    
    if (serv) {
        __serv = __wiisocket_malloc(servlen);
        if (!__serv) {
            rc = EAI_MEMORY;
            goto end;
        }
    }

    request.flags = flags;
    if (addr) {
        request.hasAddr = 1;
        memcpy(&request.addr, addr, addrlen);
    } else {
        request.hasAddr = 0;
    }

    rc = IOS_IoctlvFormat(__wiisocket_ios_heapid,
                          __wiisocket_ip_top_fd,
                          IOCTLV_SO_GETNAMEINFO,
                          "ddd:",
                          &request,
                          sizeof(request),
                          __host,
                          hostlen,
                          __serv,
                          servlen);

    strlcpy(host, __host, hostlen);
    strlcpy(serv, __serv, servlen);

end:
    free(__host);
    free(__serv);

    return rc;
}
