#include "wiisocket_internal.h"

char *
inet_ntoa(struct in_addr in)
{
    static char b[INET_ADDRSTRLEN];
    unsigned char *p = (unsigned char *)&in;

    snprintf(b, sizeof(b), "%u.%u.%u.%u",
             p[0], p[1], p[2], p[3]);

    return b;
}

