#include "wiisocket_internal.h"

/*
 * inet_aton implementation adapted from
 * bionic libc
 */
int
inet_aton(const char* cp,
          struct in_addr *inp)
{
    unsigned long parts[4];
    uint32_t result = 0;
    int saved_errno;
    size_t i;

    saved_errno = errno;
    for (i = 0; i < 4; i++) {
        char *end;
        errno = 0;
        parts[i] = strtoul(cp, &end, 0);
        if (errno || end == cp || (*end != '.' && *end != '\0')) {
            errno = saved_errno;
            return 0;
        }
        if (*end == '\0') {
            break;
        }
        cp = end + 1;
    }
    errno = saved_errno;

    switch (i) {
        case 0: /* a (a 32-bit) */
            if (parts[0] > 0xffffffff) {
                return 0;
            }
            result = parts[0];
            break;
        case 1: /* a.b (b 24-bit) */        
            if (parts[0] > 0xff || parts[1] > 0xffffff) {
                return 0;
            }
            result = (parts[0] << 24) | parts[1];
            break;
        case 2: /* a.b.c (c 16-bit) */
            if (parts[0] > 0xff || parts[1] > 0xff || parts[2] > 0xffff) {
                return 0;
            }
            result = (parts[0] << 24) | (parts[1] << 16) | parts[2];
            break;
        case 3: /* a.b.c.d (d 8-bit) */
            if (parts[0] > 0xff || parts[1] > 0xff || parts[2] > 0xff || parts[3] > 0xff) {
                return 0;
            }
            result = (parts[0] << 24) | (parts[1] << 16) | (parts[2] << 8) | parts[3];
            break;
        default:
            return 0;
    }

    if (inp) {
        inp->s_addr = htonl(result);
    }

    return 1;
}

