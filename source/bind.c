#include "wiisocket_internal.h"

struct SO_REQUEST BindRequest
{
    int sockfd;
    int hasAddr;
    struct sockaddr_storage addr;
};

int
bind(int sockfd,
     const struct sockaddr *addr,
     socklen_t addrlen)
{
    struct BindRequest request;
    int rc;

    if (__wiisocket_ip_top_fd < 0) {
        errno = ENXIO;
        return -1;
    }

    memset(&request, 0, sizeof(request));
    request.sockfd = __wiisocket_get_native_fd(sockfd);
    if (request.sockfd == -1) {
        errno = ENOTSOCK;
        return -1;
    }

    if (!addr) {
        errno = EFAULT;
        return -1;
    }

    if (addr->sa_family != AF_INET) {
        errno = EAFNOSUPPORT;
        return -1;
    }

    if (addrlen < sizeof(struct sockaddr_in)) {
        errno = EINVAL;
        return -1;
    }

    memcpy(&request.addr, addr, sizeof(struct sockaddr_in));
    request.addr.ss_len = sizeof(struct sockaddr_in);
    request.hasAddr = 1;

    rc = IOS_Ioctl(__wiisocket_ip_top_fd,
                   IOCTL_SO_BIND,
                   &request,
                   sizeof(request),
                   NULL,
                   0);

    return __wiisocket_get_result(NULL, rc);
}
