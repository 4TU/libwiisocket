#include "wiisocket_internal.h"

struct SO_REQUEST GetSockOptRequest
{
    int sockfd;
    int level;
    int optname;
    socklen_t optlen;
    unsigned char optval[20];
};

int
getsockopt(int sockfd,
           int level,
           int optname,
           void *optval,
           socklen_t *optlen)
{
    struct GetSockOptRequest request;
    int rc;

    if (__wiisocket_ip_top_fd < 0) {
        errno = ENXIO;
        return -1;
    }

    memset(&request, 0, sizeof(request));
    request.sockfd = __wiisocket_get_native_fd(sockfd);
    if (request.sockfd == -1) {
        errno = ENOTSOCK;
        return -1;
    }

    if ((optname >> 4) == 0xFFF) {
        errno = ENOTSUP;
        return -1;
    }

    if (!optval || !optlen) {
        errno = EFAULT;
        return -1;
    }

    request.level = level;
    request.optname = optname;

    rc = IOS_Ioctl(__wiisocket_ip_top_fd,
                   IOCTL_SO_GETSOCKOPT,
                   NULL,
                   0,
                   &request,
                   sizeof(request));

    if ((rc < 0) || !request.optlen) {
        return __wiisocket_get_result(NULL, rc);
    }

    if (*optlen < request.optlen) {
        errno = ENOMEM;
        return -1;
    }
    *optlen = request.optlen;

    if (optname == SO_ERROR) {
        int *sockerr = (int *)request.optval;
        int *err = (int *)optval;
        *err = __wiisocket_translate_error(*sockerr);
    } else {
        memcpy(optval, request.optval, request.optlen);
    }

    return rc;
}

