#include "wiisocket_internal.h"

struct SO_REQUEST GetAddrInfoRequestOut
{
    struct addrinfo addrinfo[0x23];
    struct sockaddr_storage addr[0x23];
    unsigned char buf[0x18];
};

int getaddrinfo(const char *node,
                const char *service,
                const struct addrinfo *hints,
                struct addrinfo **res)
{
    struct GetAddrInfoRequestOut *request;
    int rc, i,
        __node_len = 0,
        __service_len = 0;
    char *__node = NULL,
         *__service = NULL;
    struct addrinfo *__hints = NULL;

    if (__wiisocket_ip_top_fd < 0) {
        errno = ENXIO;
        return EAI_SYSTEM;
    }

    if (node) {
        __node_len = strlen(node) + 1;
        __node = __wiisocket_malloc(__node_len);
        if (!__node) {
            rc = EAI_MEMORY;
            goto end;
        }
        strlcpy(__node, node, __node_len);
    }

    if (service) {
        __service_len = strlen(service) + 1;
        __service = __wiisocket_malloc(__service_len);
        if (!__service) {
            rc = EAI_MEMORY;
            goto end;
        }
        strlcpy(__service, service, __service_len);
    }

    __hints = __wiisocket_malloc(sizeof(*__hints));
    if (!__hints) {
        rc = EAI_MEMORY;
        goto end;
    }
    if (hints) {
        memcpy(__hints, hints, sizeof(*__hints));
    } else {
        __hints->ai_socktype = 0;
        __hints->ai_protocol = 0;
        __hints->ai_flags = AI_DEFAULT;
    }
    __hints->ai_family = AF_INET;

    request = __wiisocket_malloc(sizeof(*request));
    if (!request) {
        rc = EAI_MEMORY;
        goto end;
    }

    rc = IOS_IoctlvFormat(__wiisocket_ios_heapid,
                          __wiisocket_ip_top_fd,
                          IOCTLV_SO_GETADDRINFO,
                          "ddd:d",
                          __node,
                          __node_len,
                          __service,
                          __service_len,
                          __hints,
                          sizeof(*__hints),
                          request,
                          sizeof(*request));

    if (rc < 0) {
        __wiisocket_free(request);
        goto end;
    }
    
    *res = request->addrinfo;

    request->addrinfo[0x22].ai_next = NULL;
    for (i = 0; i < 0x23; i++) {
        request->addrinfo[i].ai_addr = &request->addr[i];
        if (!request->addrinfo[i].ai_next) {
            break;
        }
        request->addrinfo[i].ai_next = &request->addrinfo[i + 1];
    }

end:
    __wiisocket_free(__hints);
    __wiisocket_free(__service);
    __wiisocket_free(__node);
    return rc;
}
