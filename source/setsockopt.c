#include "wiisocket_internal.h"

struct SO_REQUEST SetSockOptRequest
{
    int sockfd;
    int level;
    int optname;
    socklen_t optlen;
    unsigned char optval[20];
};

int
setsockopt(int sockfd,
           int level,
           int optname,
           const void *optval,
           socklen_t optlen)
{
    struct SetSockOptRequest request;
    int rc;

    if (__wiisocket_ip_top_fd < 0) {
        errno = ENXIO;
        return -1;
    }

    memset(&request, 0, sizeof(request));
    request.sockfd = __wiisocket_get_native_fd(sockfd);
    if (request.sockfd == -1) {
        errno = ENOTSOCK;
        return -1;
    }

    if ((optname >> 4) == 0xFFF) {
        errno = ENOTSUP;
        return -1;
    }

    request.level = level;
    request.optname = optname;

    if (optlen > 20) {
        errno = EINVAL;
        return -1;
    }
    request.optlen = optlen;

    if (optval && optlen) {
        memcpy(request.optval, optval, optlen);
    }

    rc = IOS_Ioctl(__wiisocket_ip_top_fd,
                   IOCTL_SO_SETSOCKOPT,
                   &request,
                   sizeof(request),
                   NULL,
                   0);

    return __wiisocket_get_result(NULL, rc);
}
