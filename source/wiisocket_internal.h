#pragma once
#include <ogc/machine/processor.h>
#include <ogc/ipc.h>
#include <ogc/lwp.h>
#include <ogc/lwp_heap.h>
#include <ogc/system.h>
#include <sys/iosupport.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <wiisocket.h>
#include <netdb.h>
#include <poll.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>
#include <fcntl.h>
#include <errno.h>

#ifdef WIISOCKET_DEBUG
#define DEBUG(...)  printf(__VA_ARGS__)
#else
#define DEBUG(...)
#endif

/* IOS NET structures */
struct __attribute__((packed))
NET_HOST_ENT
{
    char *h_name;     
    char **h_aliases; 
    int16_t h_addrtype;  
    int16_t h_length;    
    char **h_addr_list;
};

/* utility functions */
extern int      __wiisocket_get_native_fd(int fd);
extern int      __wiisocket_get_result(struct _reent *r, int rc);
extern int      __wiisocket_translate_error(int sockerrno);
struct hostent *__wiisocket_translate_hostent(struct NET_HOST_ENT *n_ent);

/* wiisocket devoptab */
extern int      __wiisocket_dev;
extern int      __wiisocket_open(struct _reent *r, void *fileStruct, const char *path, int flags, int mode);
extern int      __wiisocket_close(struct _reent *r, void *fd);
extern ssize_t  __wiisocket_write(struct _reent *r, void *fd, const char *ptr, size_t len);
extern ssize_t  __wiisocket_read(struct _reent *r, void *fd, char *ptr, size_t len);

/* IOS network stack */
extern int      __wiisocket_network_init_async(void (*cb)(int res, void *usrdata), void *usrdata);
extern void     __wiisocket_network_deinit(void);
extern int      __wiisocket_get_network_status(void);

/* wiisocket internal heap */
extern int      __wiisocket_heap_init(void);
extern void    *__wiisocket_malloc(size_t size);
extern void     __wiisocket_free(void *ptr);

/* wiisocket data for IOS */
extern int      __wiisocket_ios_heapid;
extern int      __wiisocket_ip_top_fd;

/* IOS socket request structures attributes */
#define SO_REQUEST __attribute__((packed, aligned(32)))

/* IOS socket ioctls/ioclvs */
enum NET_IOCTL
{
    IOCTL_SO_ACCEPT = 1,
    IOCTL_SO_BIND,
    IOCTL_SO_CLOSE,
    IOCTL_SO_CONNECT,
    IOCTL_SO_FCNTL,
    IOCTL_SO_GETPEERNAME,
    IOCTL_SO_GETSOCKNAME,
    IOCTL_SO_GETSOCKOPT,
    IOCTL_SO_SETSOCKOPT,
    IOCTL_SO_LISTEN,
    IOCTL_SO_POLL,
    IOCTLV_SO_RECVFROM,
    IOCTLV_SO_SENDTO,
    IOCTL_SO_SHUTDOWN,
    IOCTL_SO_SOCKET,
    IOCTL_SO_GETHOSTID,
    IOCTL_SO_GETHOSTBYNAME,
    IOCTL_SO_GETHOSTBYADDR,
    IOCTLV_SO_GETNAMEINFO,
    IOCTL_SO_GETLASTERROR,
    IOCTL_SO_INETATON,
    IOCTL_SO_INETPTON,
    IOCTL_SO_INETNTOP,
    IOCTLV_SO_GETADDRINFO,
    IOCTL_SO_SOCKATMARK,
    IOCTL_SO_STARTUP,
    IOCTL_SO_CLEANUP,
    IOCTLV_SO_GETINTERFACEOPT,
    IOCTLV_SO_SETINTERFACEOPT,
    IOCTL_SO_SETINTERFACE,
    IOCTL_SO_INITINTERFACE,
    IOCTL_SO_ICMPSOCKET = 0x30,
    IOCTLV_SO_ICMPPING,
    IOCTL_SO_ICMPCANCEL,
    IOCTL_SO_ICMPCLOSE
};
