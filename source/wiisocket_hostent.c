#include "wiisocket_internal.h"

struct hostent *
__wiisocket_translate_hostent(struct NET_HOST_ENT *n_ent)
{
    static struct hostent ent;
    unsigned ptr_offset;
    size_t i;

    ptr_offset = (unsigned)MEM_PHYSICAL_TO_K0(n_ent->h_name) - sizeof(*n_ent) - (unsigned)n_ent;

    ent.h_name = MEM_PHYSICAL_TO_K0(n_ent->h_name) - ptr_offset;
    ent.h_aliases = MEM_PHYSICAL_TO_K0(n_ent->h_aliases) - ptr_offset;
    ent.h_addr_list = MEM_PHYSICAL_TO_K0(n_ent->h_addr_list) - ptr_offset;

    for (i = 0; (i < 72) && (ent.h_aliases[i] != NULL); i++) {
        ent.h_aliases[i] = MEM_PHYSICAL_TO_K0(ent.h_aliases[i]) - ptr_offset;
    }

    for (i = 0; (i < 72) && (ent.h_addr_list[i] != NULL); i++) {
        ent.h_addr_list[i] = MEM_PHYSICAL_TO_K0(ent.h_addr_list[i]) - ptr_offset;
    }

    ent.h_addrtype = n_ent->h_addrtype;
    ent.h_length = n_ent->h_length;

    return &ent;
}
