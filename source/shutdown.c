#include "wiisocket_internal.h"

struct SO_REQUEST ShutdownRequest
{
    int sockfd;
    int how;
};

int
shutdown(int sockfd,
         int how)
{
    struct ShutdownRequest request;
    int rc;

    if (__wiisocket_ip_top_fd < 0) {
        errno = ENXIO;
        return -1;
    }

    request.sockfd = __wiisocket_get_native_fd(sockfd);
    if (request.sockfd == -1) {
        errno = ENOTSOCK;
        return -1;
    }

    request.how = how;

    rc = IOS_Ioctl(__wiisocket_ip_top_fd,
                   IOCTL_SO_SHUTDOWN,
                   &request,
                   sizeof(request),
                   NULL,
                   0);

    return __wiisocket_get_result(NULL, rc);
}
