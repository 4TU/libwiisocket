#include "wiisocket_internal.h"

int
sockatmark(int sockfd)
{
    char buf[1];
    int rc;

    rc = recv(sockfd, buf, sizeof(buf), MSG_OOB | MSG_PEEK | MSG_DONTWAIT);
    if (rc > 0) {
        return 1;
    }

    return (errno == EWOULDBLOCK) ? 0 : rc;
}
