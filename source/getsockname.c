#include "wiisocket_internal.h"

struct SO_REQUEST GetSockNameRequest
{
    int sockfd;
};

int
getsockname(int sockfd,
            struct sockaddr *addr,
            socklen_t *addrlen)
{
    struct SO_REQUEST sockaddr_storage __addr;
    struct GetSockNameRequest request;
    int rc;

    if (__wiisocket_ip_top_fd < 0) {
        errno = ENXIO;
        return -1;
    }

    request.sockfd = __wiisocket_get_native_fd(sockfd);
    if (request.sockfd == -1) {
        errno = ENOTSOCK;
        return -1;
    }

    if (!addr || !addrlen) {
        errno = EFAULT;
        return -1;
    }

    __addr.ss_len = sizeof(__addr);

    rc = IOS_Ioctl(__wiisocket_ip_top_fd, 
                   IOCTL_SO_GETSOCKNAME,
                   &request,
                   sizeof(request),
                   &__addr,
                   sizeof(__addr));

    memcpy(addr, &__addr, *addrlen);
    *addrlen = __addr.ss_len;

    return __wiisocket_get_result(NULL, rc);
}

