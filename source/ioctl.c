#include "wiisocket_internal.h"

int
ioctl(int fd,
      int request,
      ...)
{
    int flags, *nonblock;
    va_list args;

    if (request != FIONBIO) {
        errno = EINVAL;
        return -1;
    }

    va_start(args, request);
    nonblock = va_arg(args, int *);
    va_end(args);

    flags = fcntl(fd, F_GETFL, 0);
    if (flags == -1) {
        return -1;
    }

    flags = (*nonblock) ?
        (flags | O_NONBLOCK) :
        (flags & ~O_NONBLOCK);

    return fcntl(fd, F_SETFL, flags);
}
