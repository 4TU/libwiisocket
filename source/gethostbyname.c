#include "wiisocket_internal.h"

struct SO_REQUEST GetHostByNameRequestOut
{
    struct NET_HOST_ENT n_ent;
    unsigned char buf[0x450];
};

struct hostent *
gethostbyname(const char *name)
{
    static struct GetHostByNameRequestOut request;
    char *__name = NULL;
    int len, rc;

    if (__wiisocket_ip_top_fd < 0) {
        errno = ENXIO;
        return NULL;
    }

    if (!name) {
        errno = EINVAL;
        return NULL;
    }

    len = strlen(name) + 1;

    __name = __wiisocket_malloc(len);
    if (!__name) {
        errno = ENOMEM;
        return NULL;
    }

    strlcpy(__name, name, len);

    rc = IOS_Ioctl(__wiisocket_ip_top_fd,
                   IOCTL_SO_GETHOSTBYNAME,
                   __name,
                   len,
                   &request,
                   sizeof(request));

    __wiisocket_free(__name);

    if (rc < 0) {
        __wiisocket_get_result(NULL, rc);
        return NULL;
    }

    return __wiisocket_translate_hostent(&request.n_ent);
}

