#include "wiisocket_internal.h"

struct SO_REQUEST RecvFromRequest
{
    int sockfd;
    int flags;
};

ssize_t
__wiisocket_read(struct _reent *r,
                 void *fd,
                 char *ptr,
                 size_t len)
{
    struct RecvFromRequest request;
    void *__ptr;
    int rc;

    if (__wiisocket_ip_top_fd < 0) {
        r->_errno = ENXIO;
        return -1;
    }

    request.sockfd = *(int *)fd;
    request.flags = 0;

    __ptr = __wiisocket_malloc(len);
    if (__ptr == NULL) {
        r->_errno = ENOMEM;
        return -1;
    }
    memset(__ptr, 0, len);

    rc = IOS_IoctlvFormat(__wiisocket_ios_heapid,
                          __wiisocket_ip_top_fd,
                          IOCTLV_SO_RECVFROM,
                          "d:dd",
                          &request,
                          sizeof(request),
                          __ptr,
                          len,
                          NULL,
                          0);

    memcpy(ptr, __ptr, len);
    __wiisocket_free(__ptr);

    return (ssize_t)__wiisocket_get_result(r, rc);
}

