#include "wiisocket_internal.h"

#define WIISOCKET_HEAP_SIZE     (64 * 1024)

int
__wiisocket_heap_initialized = 0;

heap_cntrl
__wiisocket_heap;

void *
__wiisocket_malloc(size_t size)
{
    return __lwp_heap_allocate(&__wiisocket_heap, size);
}

void
__wiisocket_free(void *ptr)
{
    __lwp_heap_free(&__wiisocket_heap, ptr);
} 

int
__wiisocket_heap_init(void)
{
    unsigned level;
    void *__wiisocket_heap_ptr;
    
    _CPU_ISR_Disable(level);
    
    if (__wiisocket_heap_initialized)
    {
        _CPU_ISR_Restore(level);
        return 0;
    }
    
    __wiisocket_heap_ptr = (void *)(((unsigned)SYS_GetArena2Hi() - WIISOCKET_HEAP_SIZE) & ~0x1f);
    
    if (__wiisocket_heap_ptr < SYS_GetArena2Lo())
    {
        _CPU_ISR_Restore(level);
        return -1;
    }
    
    SYS_SetArena2Hi(__wiisocket_heap_ptr);
    
    __lwp_heap_init(&__wiisocket_heap,
                    __wiisocket_heap_ptr,
                    WIISOCKET_HEAP_SIZE,
                    32);
    
    __wiisocket_heap_initialized = 1;
    
    _CPU_ISR_Restore(level);
    
    return 0;
}

