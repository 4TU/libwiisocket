#include "wiisocket_internal.h"

struct SO_REQUEST SendToRequest
{
    int sockfd;
    int flags;
    int hasAddr;
    struct sockaddr_storage addr;
};

ssize_t
__wiisocket_write(struct _reent *r,
                  void *fd,
                  const char *ptr,
                  size_t len)
{
    struct SendToRequest request;
    void *__ptr;
    int rc;

    if (__wiisocket_ip_top_fd < 0) {
        r->_errno = ENXIO;
        return -1;
    }

    memset(&request, 0, sizeof(request));
    request.sockfd = *(int *)fd;

    __ptr = __wiisocket_malloc(len);
    if (__ptr == NULL) {
        r->_errno = ENOMEM;
        return -1;
    }
    memcpy(__ptr, ptr, len);

    rc = IOS_IoctlvFormat(__wiisocket_ios_heapid,
                          __wiisocket_ip_top_fd,
                          IOCTLV_SO_SENDTO,
                          "dd:",
                          __ptr,
                          len,
                          &request,
                          sizeof(request));

    __wiisocket_free(__ptr);

    return (ssize_t)__wiisocket_get_result(r, rc);
}

