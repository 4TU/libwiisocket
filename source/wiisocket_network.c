#include "wiisocket_internal.h"

/*
 * wiisocket_network.c
 * Network initialization
 * Most of this code was imported from libogc
 */


#define MAX_IP_RETRIES              100
#define MAX_INIT_RETRIES            32


#define IOCTL_NCD_GETLINKSTATUS     0x07

#define IOCTL_NWC24_STARTUP         0x06
#define IOCTL_NWC24_CLEANUP         0x07


struct SO_REQUEST NWC24Request
{
    unsigned char kd_buf[0x20];
};


struct wiisocket_network_init_data
{
    unsigned retries;
    unsigned state;
    int fd;
    unsigned char *buf;

    int prevres;
    int res;

    syswd_t alarm;

    void (*cb)(int, void *);
    void *usrdata;
};


static const char __attribute__((aligned(32)))
__manage_fs[] = "/dev/net/ncd/manage",
__iptop_fs[] = "/dev/net/ip/top",
__kd_fs[] = "/dev/net/kd/request";


int
__wiisocket_ios_heapid = -1,
__wiisocket_ip_top_fd = -1;


static volatile bool
__wiisocket_init_busy = false,
__wiisocket_init_abort = false;

static volatile int
__wiisocket_network_status = -ENETDOWN;


static int
__wiisocket_init_chain(int callres,
                       void *usrdata);


static void
__wiisocket_init_alarm(syswd_t alarm,
                       void *cb_arg)
{
    DEBUG("net_init alarm\n");
    __wiisocket_init_chain(0, cb_arg);
}


static int
__wiisocket_init_chain(int callres, void *usrdata)
{
    struct wiisocket_network_init_data *data = (struct wiisocket_network_init_data *) usrdata;
    struct timespec tb;

    DEBUG("__wiisocket_init_chain %u %d\n", data->state, callres);

    if (__wiisocket_init_abort) {
        data->state = 0xff;
        goto error;
    }

    switch (data->state) {
        case 0:
            /* open manage fd */
            data->state = 1;
            data->res = IOS_OpenAsync(__manage_fs,
                                      0,
                                      __wiisocket_init_chain,
                                      data);
            if (data->res < 0) {
                goto done;
            }

            return 0;

        case 1:
            if (callres == IPC_ENOENT) {
                DEBUG("IPC_ENOENT, retrying...\n");
                data->state = 0;
    
                tb.tv_sec = 0;
                tb.tv_nsec = 100000000;
                data->res = SYS_SetAlarm(data->alarm, &tb, __wiisocket_init_alarm, data);
                if (data->res) {
                    DEBUG("error setting the alarm: %d\n", data->res);
                    goto done;
                }

                return 0;
            } else if (callres < 0) {
                DEBUG("error opening the manage fd: %d\n", callres);
                data->res = callres;
                goto done;
            } else {
                DEBUG("opened the manage fd\n");
                data->fd = callres;
            }

            /* get link status */
            data->state = 2;
            data->res = IOS_IoctlvFormatAsync(__wiisocket_ios_heapid,
                                              data->fd,
                                              IOCTL_NCD_GETLINKSTATUS,
                                              __wiisocket_init_chain, data,
                                              ":d",
                                              data->buf, 0x20);

            if (data->res < 0) {
                data->state = 0xff;
                if (IOS_CloseAsync(data->fd, __wiisocket_init_chain, data) < 0) {
                    goto done;
                }
            }
            return 0;
            
        case 2: /* close manage fd */
            data->prevres = callres;

            data->state = 3;
            data->res = IOS_CloseAsync(data->fd, __wiisocket_init_chain, data);

            if (data->res < 0) {
                goto done;
            }

            return 0;
            
        case 3: /* open top fd */
            if (data->prevres < 0) {
                DEBUG("invalid link status %d\n", data->prevres);
                data->res = data->prevres;
                goto done;
            }

            data->state = 4;
            data->res = IOS_OpenAsync(__iptop_fs, 0,
                                      __wiisocket_init_chain, data);

            if (data->res < 0) {
                goto done;
            }

            return 0;
            
        case 4: /* open request fd */
            if (callres < 0) {
                DEBUG("error opening the top fd: %d\n", callres);
                data->res = callres;
                goto done;
            } else {
                __wiisocket_ip_top_fd = callres;
            }

            data->state = 5;
            data->res = IOS_OpenAsync(__kd_fs, 0,
                                      __wiisocket_init_chain, data);

            if (data->res < 0) {
                goto error;
            }

            return 0;
            
        case 5: /* NWC24 startup */
            if (callres < 0) {
                DEBUG("error opening the request fd: %d\n", callres);
                data->res = callres;
                goto error;
            }
            
            data->fd = callres;
            data->retries = MAX_INIT_RETRIES;

        case 6:
            data->state = 7;
            data->res = IOS_IoctlAsync(data->fd,
                                       IOCTL_NWC24_STARTUP,
                                       NULL, 0,
                                       data->buf, 0x20,
                                       __wiisocket_init_chain, data);

            if (data->res < 0) {
                data->state = 0xff;
                if (IOS_CloseAsync(data->fd, __wiisocket_init_chain, data) < 0) {
                    goto done;
                }
            }

            return 0;

        case 7:
            if (callres == 0) {
                memcpy(&callres, data->buf, sizeof(callres));
                if (callres == -29 && --data->retries) {
                    data->state = 6;

                    tb.tv_sec = 0;
                    tb.tv_nsec = 100000000;
                    data->res = SYS_SetAlarm(data->alarm, &tb, __wiisocket_init_alarm, data);
                    if (data->res) {
                        DEBUG("error setting the alarm: %d\n", data->res);
                        data->state = 0xff;
                        if (IOS_CloseAsync(data->fd, __wiisocket_init_chain, data) < 0) {
                            goto error;
                        }
                    }

                    return 0;

                } else if (callres == -15) {
                    /* this happens if it's already been started */
                    callres = 0;
                }
            }
            data->prevres = callres;

            data->state = 8;
            data->res = IOS_CloseAsync(data->fd, __wiisocket_init_chain, data);
            if (data->res < 0) {
                goto error;
            }
    
            return 0;

        case 8: /* socket startup */
            if (data->prevres < 0) {
                DEBUG("NWC24 startup failed: %d\n", data->prevres);
                data->res = data->prevres;
                goto error;
            }

            data->state = 9;
            data->retries = MAX_IP_RETRIES;
            data->res = IOS_IoctlAsync(__wiisocket_ip_top_fd,
                                       IOCTL_SO_INITINTERFACE,
                                       NULL, 0,
                                       NULL, 0,
                                       __wiisocket_init_chain, data);

            if (data->res < 0) {
                goto error;
            }

            return 0;

        case 9: /* check ip */
            if (callres < 0) {
                data->res = callres;
                DEBUG("socket startup failed: %d\n", data->res);
                goto error;
            }

            data->state = 10;
            data->res = IOS_IoctlAsync(__wiisocket_ip_top_fd,
                                       IOCTL_SO_GETHOSTID,
                                       NULL, 0,
                                       NULL, 0,
                                       __wiisocket_init_chain, data);

            if (data->res < 0) {
                goto error;
            }

            return 0;

        case 10: /* done, check callres */
            if (callres == 0) {
                if (!data->retries) {
                    DEBUG("unable to obtain ip\n");
                    data->res = -ETIMEDOUT;
                    goto error;
                }

                DEBUG("unable to obtain ip, retrying...\n");

                data->state = 9;
                data->retries--;

                tb.tv_sec = 0;
                tb.tv_nsec = 100000000;
                data->res = SYS_SetAlarm(data->alarm, &tb, __wiisocket_init_alarm, data);
                if (data->res) {
                    DEBUG("error setting the alarm: %d\n", data->res);
                    goto error;
                }

                return 0;
            }

            data->res = 0;
            goto done;

        error:
        case 0xff: /* error occured before, last async call finished */
            data->state = 0xff;

            if (__wiisocket_ip_top_fd < 0) {
                goto done;
            }

            data->fd = __wiisocket_ip_top_fd;
            __wiisocket_ip_top_fd = -1;

            /* will call error again */
            if (IOS_CloseAsync(data->fd, __wiisocket_init_chain, data) < 0) {
                goto done;
            }

            return 0;

        default:
            DEBUG("unknown state in chain %d\n", data->state);
            data->res = -1;
            goto done;
    }

done:
    SYS_RemoveAlarm(data->alarm);

    __wiisocket_network_status = data->res;
    
    if (data->cb) {
        data->cb(data->res, data->usrdata);
    }

    free(data->buf);
    free(data);

    __wiisocket_init_busy = false;

    return 0;
}


static void
__wiisocket_wc24_cleanup(void)
{
    struct NWC24Request request;
    int kd_fd;
    
    kd_fd = IOS_Open(__kd_fs, 0);
    if (kd_fd < 0) {
        return;
    }
    
    IOS_Ioctl(kd_fd,
              IOCTL_NWC24_CLEANUP,
              NULL, 0,
              &request, sizeof(request));
    
    IOS_Close(kd_fd);
}



int
__wiisocket_network_init_async(void (*cb)(int res, void *usrdata),
                               void *usrdata)
{
    struct wiisocket_network_init_data *data;
    int rc;
    
    if (__wiisocket_ip_top_fd >= 0) {
        cb(0, usrdata);
        return 1;
    }
    
    if (__wiisocket_init_busy) {
        errno = -EBUSY;
        return -1;
    }

    /* init heap for wiisocket allocations */
    if ((rc = __wiisocket_heap_init()) < 0) {
        DEBUG("__wiisocket_heap_init: %d\n", rc);
        return -2;
    }
    
    /* init ios heap used for wiisocket calls */
    if (__wiisocket_ios_heapid < 0) {
        __wiisocket_ios_heapid = iosCreateHeap(1024);
    }
    if (__wiisocket_ios_heapid < 0) {
        DEBUG("iosCreateHeap: %d\n", __wiisocket_ios_heapid);
        return -3;
    }

    /* prepare init chain data */
    data = calloc(1, sizeof(*data));
    if (!data) {
        return -4;
    }

    if (SYS_CreateAlarm(&data->alarm)) {
        DEBUG("error creating alarm\n");
        free(data);
        return -5;
    }

    data->buf = memalign(32, 32);
    if (!data->buf) {
        free(data);
        return -6;
    }

    data->cb = cb;
    data->usrdata = usrdata;

    /* kick off the callback chain */
    __wiisocket_init_busy = true;
    __wiisocket_init_abort = false;
    __wiisocket_network_status = -EBUSY;

    __wiisocket_init_chain(IPC_ENOENT, data);
    
    return 0;
}

int
__wiisocket_get_network_status(void)
{
    return __wiisocket_network_status;
}

void
__wiisocket_network_deinit(void)
{
    /* cancel initialization if in progress */
    if (__wiisocket_init_busy) {
        __wiisocket_init_abort = true;
        while (__wiisocket_init_busy) {
            usleep(50);
        }
    }

    /* close ip top handle */
    if (__wiisocket_ip_top_fd >= 0) {
        IOS_Close(__wiisocket_ip_top_fd);
    }
    __wiisocket_ip_top_fd = -1;

    /* wc24 socket cleanup */
    __wiisocket_wc24_cleanup();

    /* network down */
    __wiisocket_network_status = -ENETDOWN;
}
