#include "wiisocket_internal.h"

/*
 * Copyright (c) 1996,1999 by Internet Software Consortium.
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND INTERNET SOFTWARE CONSORTIUM DISCLAIMS
 * ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL INTERNET SOFTWARE
 * CONSORTIUM BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
 * ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 */

int
inet_pton(int af,
          const char *src,
          void *dst)
{
    static const char digits[] = "0123456789";
    int saw_digit = 0, octets = 0, ch;
    unsigned char buf[sizeof(struct in_addr)], *tp = buf;

    if (af != AF_INET) {
        errno = EAFNOSUPPORT;
        return -1;
    }

    saw_digit = 0;
    octets = 0;

    *tp = 0;
    while ((ch = *src++) != '\0') {
        const char *pch;

        if ((pch = strchr(digits, ch)) != NULL) {
            unsigned int new = *tp * 10 + (pch - digits);

            if (new > 255) {
                return 0;
            }

            if (! saw_digit) {
                if (++octets > 4) {
                    return 0;
                }
                saw_digit = 1;
            }

            *tp = new;
        } else if (ch == '.' && saw_digit) {
            if (octets == 4) {
                return 0;
            }

            *++tp = 0;
            saw_digit = 0;
        } else {
            return 0;
        }
    }

    if (octets < 4) {
        return 0;
    }

    memcpy(dst, buf, sizeof(struct in_addr));
    return 1;
}
