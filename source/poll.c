#include "wiisocket_internal.h"

struct SO_REQUEST PollRequest
{
    int64_t timeout;
};

int
poll(struct pollfd *fds,
     nfds_t nfds,
     int timeout)
{
    struct PollRequest request;
    struct pollfd *__fds;
    size_t __fds_size;
    nfds_t i;
    int rc;

    if (__wiisocket_ip_top_fd < 0) {
        errno = ENXIO;
        return -1;
    }

    if (!fds || !nfds) {
        errno = EINVAL;
        return -1;
    }

    __fds_size = nfds * sizeof(struct pollfd);
    __fds = __wiisocket_malloc(__fds_size);
    if (!__fds) {
        errno = ENOMEM;
        return -1;
    }

    for (i = 0; i < nfds; i++) {
        __fds[i].fd = __wiisocket_get_native_fd(fds[i].fd);
        __fds[i].events = fds[i].events;
    }
    request.timeout = (int64_t)timeout;

    rc = IOS_Ioctl(__wiisocket_ip_top_fd,
                   IOCTL_SO_POLL,
                   &request,
                   sizeof(request),
                   __fds,
                   __fds_size);

    for (i = 0; i < nfds; i++) {
        fds[i].revents = __fds[i].revents;
    }

    __wiisocket_free(__fds);
    return __wiisocket_get_result(NULL, rc);
}
