#include "wiisocket_internal.h"

in_addr_t
inet_addr(const char* cp)
{
    struct in_addr addr;
    return inet_aton(cp, &addr) ?
        addr.s_addr : INADDR_NONE;
}
