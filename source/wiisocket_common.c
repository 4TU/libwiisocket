#include "wiisocket_internal.h"

#define WIISOCKET_UNKNOWN_ERROR_OFFSET 10000

static unsigned char
__wiisocket_error_code_map[] =
{
    0,
    E2BIG,
    EACCES,
    EADDRINUSE,
    EADDRNOTAVAIL,
    EAFNOSUPPORT,
    EAGAIN,
    EALREADY,
    EBADF,
    EBADMSG,
    EBUSY,
    ECANCELED,
    ECHILD,
    ECONNABORTED,
    ECONNREFUSED,
    ECONNRESET,
    EDEADLK,
    EDESTADDRREQ,
    EDOM,
    EDQUOT,
    EEXIST,
    EFAULT,
    EFBIG,
    EHOSTUNREACH,
    EIDRM,
    EILSEQ,
    EINPROGRESS,
    EINTR,
    EINVAL,
    EIO,
    EISCONN,
    EISDIR,
    ELOOP,
    EMFILE,
    EMLINK,
    EMSGSIZE,
    EMULTIHOP,
    ENAMETOOLONG,
    ENETDOWN,
    ENETRESET,
    ENETUNREACH,
    ENFILE,
    ENOBUFS,
    ENODATA,
    ENODEV,
    ENOENT,
    ENOEXEC,
    ENOLCK,
    ENOLINK,
    ENOMEM,
    ENOMSG,
    ENOPROTOOPT,
    ENOSPC,
    ENOSR,
    ENOSTR,
    ENOSYS,
    ENOTCONN,
    ENOTDIR,
    ENOTEMPTY,
    ENOTSOCK,
    ENOTSUP,
    ENOTTY,
    ENXIO,
    EOPNOTSUPP,
    EOVERFLOW,
    EPERM,
    EPIPE,
    EPROTO,
    EPROTONOSUPPORT,
    EPROTOTYPE,
    ERANGE,
    EROFS,
    ESPIPE,
    ESRCH,
    ESTALE,
    ETIME,
    ETIMEDOUT,
    ETXTBSY,
    EXDEV
};

int
__wiisocket_get_native_fd(int fd)
{
    __handle *handle = __get_handle(fd);
    if (handle == NULL) {
        errno = EBADF;
        return -1;
    }
    if (strcmp(devoptab_list[handle->device]->name, "soc") != 0) {
        errno = ENOTSOCK;
        return -1;
    }
    return *(int *)handle->fileStruct;
}

int
__wiisocket_translate_error(int sockerrno)
{
    if (sockerrno <= 0) {
        return 0;
    }
    if (sockerrno < sizeof(__wiisocket_error_code_map)) {
        return __wiisocket_error_code_map[sockerrno];
    }
    return WIISOCKET_UNKNOWN_ERROR_OFFSET + sockerrno;
}

int
__wiisocket_get_result(struct _reent *r,
                       int rc)
{
    int sockerror, error;

    if (rc >= 0) {
        return rc;
    }

    sockerror = -rc;
    error = __wiisocket_translate_error(sockerror);

    if (r) {
        r->_errno = error;
    } else {
        errno = error;
    }

    return -1;
}

