#include "wiisocket_internal.h"

long
gethostid(void)
{
    long hostid, retries;

    if (__wiisocket_ip_top_fd < 0) {
        errno = ENXIO;
        return -1;
    }

    for (hostid = 0, retries = 0;
         !hostid && (retries < 5);
         usleep(100000), retries++)
    {
        hostid = IOS_Ioctl(__wiisocket_ip_top_fd,
                           IOCTL_SO_GETHOSTID,
                           NULL, 0,
                           NULL, 0);
    }

    return hostid;
}

