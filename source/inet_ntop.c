#include "wiisocket_internal.h"

const char *
inet_ntop(int af,
          const void *src,
          char *dst,
          socklen_t size)
{
    const unsigned char *p = (const unsigned char *)src;
    char b[INET_ADDRSTRLEN];
    int l;

    if (af != AF_INET) {
        errno = EAFNOSUPPORT;
        return NULL;
    }

    l = snprintf(b, sizeof(b), "%u.%u.%u.%u",
                 p[0], p[1], p[2], p[3]);

    if ((l <= 0) || (l >= size)) {
        errno = ENOSPC;
        return NULL;
    }

    strlcpy(dst, b, size);
    return dst;
}

