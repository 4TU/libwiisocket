#-------------------------------------------------------------------------------
.SUFFIXES:
#-------------------------------------------------------------------------------

ifeq ($(strip $(DEVKITPRO)),)
$(error "Please set DEVKITPRO in your environment. export DEVKITPRO=<path to>/devkitpro")
endif

TOPDIR ?= $(CURDIR)

include $(DEVKITPPC)/wii_rules

#-------------------------------------------------------------------------------
# library version
#-------------------------------------------------------------------------------
VERSION		:=	0.1

#-------------------------------------------------------------------------------
# TARGET is the name of the output
# BUILD is the directory where object files & intermediate files will be placed
# SOURCES is a list of directories containing source code
# INCLUDES is a list of directories containing header files
# INSTALL is the directory where the library will be installed
#-------------------------------------------------------------------------------
TARGET		:=	libwiisocket
BUILD		:=	build
SOURCES		:=	source
INCLUDES	:=	include

# Here be dragons
INSTALL		?=	$(DEVKITPRO)/libogc

#-------------------------------------------------------------------------------
# options for code generation
#-------------------------------------------------------------------------------
CFLAGS		:=	-g -O2 -Wall $(MACHDEP) $(INCLUDE) $(DEFINES) \
			-D__LINUX_ERRNO_EXTENSIONS__

CXXFLAGS	:=	$(CFLAGS)

ASFLAGS		:=	$(MACHDEP)

#-------------------------------------------------------------------------------
# list of directories containing libraries, this must be the top level
# containing include and lib
#-------------------------------------------------------------------------------
LIBDIRS		:=	

#-------------------------------------------------------------------------------
# no real need to edit anything past this point unless you need to add additional
# rules for different file extensions
#-------------------------------------------------------------------------------
ifneq ($(BUILD),$(notdir $(CURDIR)))
#-------------------------------------------------------------------------------

export OUTPUT	:=	$(CURDIR)/$(TARGET)
export TOPDIR	:=	$(CURDIR)

export VPATH	:=	$(foreach dir,$(SOURCES),$(CURDIR)/$(dir))
export DEPSDIR	:=	$(CURDIR)/$(BUILD)

CFILES		:=	$(foreach dir,$(SOURCES),$(notdir $(wildcard $(dir)/*.c)))
CPPFILES	:=	$(foreach dir,$(SOURCES),$(notdir $(wildcard $(dir)/*.cpp)))
SFILES		:=	$(foreach dir,$(SOURCES),$(notdir $(wildcard $(dir)/*.s)))

#-------------------------------------------------------------------------------
# use CXX for linking C++ projects, CC for standard C
#-------------------------------------------------------------------------------
ifeq ($(strip $(CPPFILES)),)
#-------------------------------------------------------------------------------
	export LD	:=	$(CC)
#-------------------------------------------------------------------------------
else
#-------------------------------------------------------------------------------
	export LD	:=	$(CXX)
#-------------------------------------------------------------------------------
endif
#-------------------------------------------------------------------------------

export SRCFILES		:=	$(CPPFILES) $(CFILES) $(SFILES)
export OFILES		:=	$(CPPFILES:.cpp=.o) $(CFILES:.c=.o) $(SFILES:.s=.o)
export INCLUDE		:=	$(foreach dir,$(INCLUDES),-I$(CURDIR)/$(dir)) \
				$(foreach dir,$(LIBDIRS),-I$(dir)/include) \
				-I$(CURDIR)/$(BUILD) \
				-I$(LIBOGC_INC)


.PHONY: dist-bin dist-src dist install all clean

#-------------------------------------------------------------------------------

dist-bin: all
	@[ -d lib/wii ] || mkdir -p lib/wii
	@cp $(TARGET).a lib/wii/
	@tar --exclude=*~ -cjf $(TARGET)-$(VERSION).tar.bz2 include lib

dist-src:
	@tar --exclude=*~ -cjf $(TARGET)-src-$(VERSION).tar.bz2 include source Makefile README.md

dist: dist-src dist-bin

install: dist-bin
	mkdir -p $(DESTDIR)$(INSTALL)
	bzip2 -cd $(TARGET)-$(VERSION).tar.bz2 | tar -xf - -C $(DESTDIR)$(INSTALL)

all: $(SRCFILES) $(BUILD)
	@$(MAKE) --no-print-directory -C $(BUILD) -f $(CURDIR)/Makefile

$(BUILD):
	@[ -d $@ ] || mkdir -p $@

#-------------------------------------------------------------------------------
clean:
	@echo clean ...
	@rm -rf $(BUILD) $(TARGET).a lib

#-------------------------------------------------------------------------------
else
.PHONY:	all

#-------------------------------------------------------------------------------
# main targets
#-------------------------------------------------------------------------------
all		:	$(OUTPUT).a

$(OUTPUT).a	:	$(OFILES)

#-------------------------------------------------------------------------------
endif
#------------------------------------------------------------------------------- 
